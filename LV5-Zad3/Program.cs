﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data1 = new Dataset("marko.txt");


            User user1 = User.GenerateUser("Ivo");
            User user2 = User.GenerateUser("Mirko");

            ProtectionProxyDataset proxy1 = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset proxy2 = new ProtectionProxyDataset(user2);


            VirtualProxyDataset proxy6 = new VirtualProxyDataset("marko.txt");


            DataConsolePrinter printer = new DataConsolePrinter();
            printer.Print(proxy1);
            Console.WriteLine();
            printer.Print(proxy2);
            Console.WriteLine();









        }
    }
}
