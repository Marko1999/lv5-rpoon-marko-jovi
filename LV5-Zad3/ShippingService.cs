﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5
{
    class ShippingService
    {
        private double PayPerWeight;

        public ShippingService(double PayPerWeight) { this.PayPerWeight = PayPerWeight; }

        public double PostPrice(double Weight)
        {
            return Weight * PayPerWeight;
        }

    }
}
