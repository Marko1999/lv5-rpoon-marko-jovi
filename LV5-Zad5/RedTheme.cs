﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace LV5_Zad5
{
    class RedTheme : ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.Red;
        }

        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.White;

        }

        public string GetHeader(int width)
        {
            return new string('+', width);


        }

        public string GetFooter(int width)
        {
            return new string('_', width);

        }









    }
}
