﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_Zad5
{
    class GroupReminder : Note
    {
        private List<String> names;
        public GroupReminder(string message, ITheme theme) : base(message, theme) { names = new List<String>(); }




        public void NoteInput(string name)
        {
            names.Add(name);
        }



        public void NoteRemove(string name)
        {
            names.Remove(name);
        }




        public override void Show()
        {
            this.ChangeColor();

            Console.WriteLine("Notes: ");

            string framedMessage = this.GetFramedMessage();

            Console.WriteLine(framedMessage);







            foreach (String k in names)
            {

                this.ChangeColor();
                Console.WriteLine(k);
                Console.ResetColor();

            }
        }
    }
}